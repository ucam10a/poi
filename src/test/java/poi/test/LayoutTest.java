package poi.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import domain.ClassData;
import domain.Student;
import poi.ComplexLayout;
import poi.ConsoleDebugPrinter;
import poi.ObjectConstructor;

public class LayoutTest {

    public static void main(String[] args) throws Exception {
        
        ComplexLayout layout = new ComplexLayout();
        
        ClassData data = new ClassData();
        data.setTeacherName("Teacher");
        data.setOffice("110");
        data.setTel("1234567");
        
        for (int i = 0; i < 10; i++) {
            Student student = new Student();
            student.setId("" + i);
            student.setName("Student" + i);
            student.setColledge("Colledge" + i);
            data.getStudents().add(student);
        }
        
        List<ClassData> list = new ArrayList<ClassData>();
        for (int i = 0; i < 20; i++) {
            data.setSheetName("Teacher" + i);
            data.setTeacherName("Teacher" + i);
            ClassData newData = ObjectConstructor.construct(data, ClassData.class);
            list.add(newData);
        }
        
        File template = new File("E:/myClass.xlsx");
        Workbook wb1 = layout.exportList(template, list, ClassData.class);
        
        ComplexLayout.saveFile(wb1, new File("E:/myTest.xlsx"));
        
        ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
        Workbook wb2 = ComplexLayout.getWorkbook(new File("E:/myTest.xlsx"));
        List<ClassData> retList = layout.parseList(wb2, ClassData.class);
        printer.printObjectParam("retList", retList);
        
    }

}
