package poi.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import csv.EasyCsvParser;
import csv.Model;
import poi.ConsoleDebugPrinter;

public class Test {

    public static void main(String[] args) throws Exception {

        File file = new File("test-1.csv");
        EasyCsvParser parser = new EasyCsvParser(file);
        
//        List<String> test = new ArrayList<String>();
//        test.add("Make,Model,Description,Price,Remark");
//        test.add("Dell,P3421W,\"Dell 34, Curved, USB-C Monitor\",2499.00,\"123\"");
//        test.add("Dell,P3401W,\"Dell 46, Curved, USB-C Monitor\",2699.00,");
//        test.add("Dell,\"\",\"Alienware 38 Curved \"\"Gaming Monitor\"\"\",6699.00,");
//        test.add("Samsung,,\"49\"\" Dual QHD, QLED, HDR1000\",6199.00,bc");
//        test.add("Samsung,,\"Promotion! Special Price ");
//        test.add("49\"\" Dual QHD, QLED, HDR1000\",4999.00,az");
//        EasyCsvParser parser = new EasyCsvParser(test);
        
        parser.setWithHeader(true);

        ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
        printer.setShowFieldType(true);

        /*
         * List<List<Object>> result = parser.parseList();
         * System.out.println("headers:"); for (String header : parser.headers)
         * { System.out.println(header); }
         * System.out.println("------------------------------");
         * 
         * for (List<Object> list : result) { //printer.printObjectParam("list",
         * list); for (Object obj : list) { System.out.println(obj); }
         * System.out.println("------------------------------"); }
         */

        List<Model> models = parser.parse(Model.class);
        printer.printObjectParam("models", models);
        
        EasyCsvParser.exportCsv("test-2.csv", models, Model.class, true);

    }

}
