package poi.test;

import java.util.Arrays;

import poi.CIDRTool;

public class IpTest {

    public static void main(String args[]) throws Exception
    {
        
        System.out.println(Arrays.asList(CIDRTool.cidrToRange("10.0.1.12/26")));
        System.out.println(CIDRTool.isInRange("10.0.1.12/26", "10.0.1.10"));
    
    }
    
}
