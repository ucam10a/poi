package poi.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import domain.Student;
import poi.SimpleLayout;

public class SimpleLayoutTest {

    public static void main(String[] args) throws Exception {
         
        
        List<Student> list = new ArrayList<Student>();
        for (int i = 0; i < 10; i++) {
            Student student = new Student();
            student.setId("" + i);
            student.setName("Student" + i);
            student.setColledge("Colledge" + i);
            list.add(student);
        }
        
        SimpleLayout layout = new SimpleLayout();
        Workbook wb = layout.exportList(list, Student.class);
        
        SimpleLayout.saveFile(wb, new File("E:/myTest.xlsx"));
        
    }

}
