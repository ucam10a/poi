package poi.test;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import poi.Calculate;
import poi.ConsoleDebugPrinter;

public class CalculateTest {

    public static void main(String[] args) throws Exception {
        
        ConsoleDebugPrinter printer = new ConsoleDebugPrinter();
        FileInputStream fis = new FileInputStream("D:/frank/Book2.xlsx");
        Workbook wb = new XSSFWorkbook(fis);
        
//        for (int i = 1; i < 10; i++) {
//            Map<String, Object> dataSource = new HashMap<String, Object>();
//            dataSource.put("a_score", 10 * i);
//            dataSource.put("b_score", 10 * (i + 1));
//            CellValue cellValue = calculateSingleRow(wb, dataSource);
//            print(cellValue);
//        }
        
        for (int i = 1; i < 5; i++) {
            Map<String, List<Object>> dataSource = new HashMap<String, List<Object>>();
            List<Object> values = new ArrayList<Object>();
            
            Random ran = new Random(System.currentTimeMillis());
            int valNum = ran.nextInt(9) + 1;
            for (int j = 0; j < valNum; j++) {
                int val = ran.nextInt(9) + 1;
                values.add(val);
            }
            dataSource.put("a_score", values);
            
            Thread.sleep(100);
            ran = new Random(System.currentTimeMillis());
            values = new ArrayList<Object>();
            valNum = ran.nextInt(9) + 1;
            for (int j = 0; j < valNum; j++) {
                int val = ran.nextInt(9) + 1;
                values.add(val);
            }
            dataSource.put("b_score", values);
            
            CellValue cellValue = Calculate.calculateMultiRows(wb, dataSource);
            printer.printObjectParam("dataSource", dataSource);
            Calculate.print(cellValue);
        }
        
    }

}
