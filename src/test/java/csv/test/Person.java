package csv.test;

import com.opencsv.bean.CsvBindByName;

public class Person {

    @CsvBindByName(column = "My name")
    private String name;
    
    @CsvBindByName
    private String address;
    
    @CsvBindByName
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
}
