package process.test;

import java.util.ArrayList;
import java.util.List;

import domain.User;
import poi.DataProcessor;

public class Test {

    public static void main(String[] args) throws Exception {
        
        List<String> passHistory = new ArrayList<String>();
        passHistory.add("123456789");
        passHistory.add("987654321");
        
        User user1 = new User();
        user1.setUsername("bob");
        user1.setPassword("12345");
        DataProcessor.process(user1);
        System.out.println("use crediential check: " + user1.isAllowLogin());
        
        User user2 = new User();
        user2.setUsername("tom");
        user2.setPassword("987654321");
        DataProcessor.process(user2);
        System.out.println("use crediential check: " + user2.isAllowLogin());
        DataProcessor.process(user2, passHistory);
        System.out.println("use crediential check: " + user2.isAllowLogin());
        
    }
    
}
