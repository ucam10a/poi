package poi;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.CellType;

public class SimpleLayout extends AbstractLayout implements TableLayout {
    
    public static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;
    public static final short EXCEL_ROW_HEIGHT_FACTOR = 20;
    public static final int UNIT_OFFSET_LENGTH = 7;
    public static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };
    
    public <T> List<T> parseList(Sheet sheet, Class<T> cls) throws Exception {
        List<T> result = new ArrayList<T>();
        Row headerRow = sheet.getRow(0);
        Field[] fields = cls.getDeclaredFields();
        int lastCol = getLastColIdx(headerRow);
        Map<Integer, String> headerMap = getHeaderMap(headerRow, cls);
        int lastRow = getLastRow(sheet, fields.length);
        for (int i = 1; i <= lastRow; i++) {
            T bean = cls.getDeclaredConstructor().newInstance();
            Row row = sheet.getRow(i);
            boolean allNull = true;
            if (row != null) {
                for (int j = 0; j <= lastCol; j++) {
                    Cell cell = row.getCell(j);
                    String fieldName = headerMap.get(j);
                    Object val = null;
                    if (cell != null && fieldName != null) {
                        val = getCellValue(cell);
                        setBeanValue(bean, fieldName, val);
                    }
                    if (val != null && !"".equals(val)) {
                        allNull = false;
                    }
                }
            }
            if (allNull == false) {
                result.add(bean);
            }
        }
        return result;
    }
    
    public <T> Workbook exportList(List<T> list, Class<T> cls) throws Exception {
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        Map<Integer, String> headerMap = getHeaderMap(cls);
        int rowIdx = 0;
        setColWidth(sheet, cls);
        createHeader(sheet.createRow(rowIdx), cls);
        rowIdx++;
        if (list == null || list.size() == 0) {
            return wb;
        } else {
            for (T bean : list) {
                Row row = sheet.createRow(rowIdx);
                setBeanValueToRow(wb, row, bean, headerMap);
                rowIdx++;
            }
        }
        return wb;
    }

    private void setColWidth(Sheet sheet, Class<?> cls) {
        Field[] fields = cls.getDeclaredFields();
        int colIdx = 0;
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                short width = info.width();
                if (width > 0) {
                    int pixWidth = pixel2WidthUnits(width);
                    sheet.setColumnWidth(colIdx, pixWidth);
                }
                colIdx++;
            }
        }
    }

    private static void createHeader(Row row, Class<?> cls) {
        Field[] fields = cls.getDeclaredFields();
        int colIdx = 0;
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                Cell cell = row.createCell(colIdx);
                String title = info.title();
                setCellValue(cell, title);
                colIdx++;
            }
        }
    }

    private static Map<Integer, String> getHeaderMap(Row headerRow, Class<?> cls) {
        Map<Integer, String> headerMap = new HashMap<Integer, String>();
        Field[] fields = cls.getDeclaredFields();
        int lastCol = getLastColIdx(headerRow);
        for (int i = 0; i <= lastCol; i++) {
            Cell cell = headerRow.getCell(i);
            if (cell != null) {
                Object val = getCellValue(cell);
                if (val != null && val.getClass() == String.class) {
                    String title = val.toString();
                    if (!"".equals(title)) {
                        for (Field f : fields) {
                            PoiInfo info = f.getAnnotation(PoiInfo.class);
                            if (info != null) {
                                if (title.equalsIgnoreCase(info.title())) {
                                    headerMap.put(i, f.getName());
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return headerMap;
    }

    private static Map<Integer, String> getHeaderMap(Class<?> cls) {
        Map<Integer, String> headerMap = new HashMap<Integer, String>();
        Field[] fields = cls.getDeclaredFields();
        int cntIdx = 0;
        for (Field field : fields) {
            PoiInfo info = field.getAnnotation(PoiInfo.class);
            if (info != null) {
                headerMap.put(cntIdx, field.getName());
                cntIdx++;
            }
        }
        return headerMap;
    }
    
    private static void setBeanValueToRow(Workbook wb, Row row, Object bean, Map<Integer, String> headerMap) throws Exception {
        CellStyle dateStyle = getDateStyle(wb);
        for (int id : headerMap.keySet()) {
            Cell cell = row.createCell(id);
            String fieldName = headerMap.get(id);
            Object val = PlainObjectOperator.runGetter(bean, fieldName);
            setCellValue(cell, val, dateStyle);
        }
    }
    
    public static short pixel2WidthUnits(int pxs) {
        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * (pxs / UNIT_OFFSET_LENGTH));
        widthUnits += UNIT_OFFSET_MAP[(pxs % UNIT_OFFSET_LENGTH)];
        return widthUnits;
    }

    public static int widthUnits2Pixel(short widthUnits) {
        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH;
        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR;
        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));
        return pixels;
    }

    public static int heightUnits2Pixel(short heightUnits) {
        int pixels = (heightUnits / EXCEL_ROW_HEIGHT_FACTOR);
        int offsetWidthUnits = heightUnits % EXCEL_ROW_HEIGHT_FACTOR;
        pixels += Math.floor((float) offsetWidthUnits / ((float) EXCEL_ROW_HEIGHT_FACTOR / UNIT_OFFSET_LENGTH));
        return pixels;
    }
    
    private static int getLastColIdx(Row headerRow) {
        int ret = 0;
        Cell cell = headerRow.getCell(0);
        Cell nxtCell = headerRow.getCell(1);
        if (cell == null && nxtCell == null) {
            return 0;
        }
        while (ret < 10000) {
            cell = headerRow.getCell(ret);
            nxtCell = headerRow.getCell(ret + 1);
            if (cell == null && nxtCell == null) {
                return ret - 1;
            }
            if (nxtCell == null) {
                return ret;
            }
            if (cell.getCellType() == CellType.BLANK && nxtCell.getCellType() == CellType.BLANK) {
                return ret - 1;
            }
            ret++;
        }
        return ret;
    }
    
}
