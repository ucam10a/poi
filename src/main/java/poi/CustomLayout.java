package poi;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public interface CustomLayout {

    public abstract <T> T parse(Sheet sheet, Class<T> cls) throws Exception;
    
    public abstract <T> List<T> parseList(Workbook wb, Class<T> cls) throws Exception;
    
    public abstract <T> Workbook export(T data, Class<T> cls) throws Exception;
    
}
