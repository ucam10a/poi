package poi;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ComplexLayout extends AbstractLayout implements CustomLayout {
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T> T parse(Sheet sheet, Class<T> cls) throws Exception {
        T result = cls.getDeclaredConstructor().newInstance();
        Map<String, Field> cellRefFieldMap = getCellRefField(cls);
        for (Entry<String, Field> entry : cellRefFieldMap.entrySet()) {
            String ref = entry.getKey();
            Field field = entry.getValue();
            CellReference refCell = new CellReference(ref);
            Row row = sheet.getRow(refCell.getRow());
            if (row == null) {
                continue;
            }
            Cell cell = row.getCell(refCell.getCol());
            if (cell == null) {
                continue;
            }
            Object val = getCellValue(cell);
            if (val != null) {
                if (nConverter.isNumber(val.getClass())) {
                    Object dVal = nConverter.Convert(val, field.getType());
                    PlainObjectOperator.runSetter(result, field.getName(), dVal);
                } else if (tConverter.isTime(val.getClass())) {
                    Object dt = tConverter.Convert(val, field.getType());
                    PlainObjectOperator.runSetter(result, field.getName(), dt);
                } else {
                    if (field.getType() == Boolean.class) {
                        PlainObjectOperator.runSetter(result, field.getName(), Boolean.valueOf(val.toString()));
                    } else {
                        PlainObjectOperator.runSetter(result, field.getName(), val.toString());
                    }
                }
            }
        }
        Map<String, Field> rowRefFieldMap = getRowRefField(cls);
        for (Entry<String, Field> entry : rowRefFieldMap.entrySet()) {
            String ref = entry.getKey();
            Field field = entry.getValue();
            CellReference refCell = new CellReference(ref);
            Row row = sheet.getRow(refCell.getRow());
            if (row == null) {
                continue;
            }
            Object rObj = null;
            String clsType = field.getType().toString();
            if (clsType.contains("java.util.List")) {
                rObj = new ArrayList<>();
            } else if (clsType.contains("java.util.Set")) {
                rObj = new HashSet<>();
            } else {
                rObj = field.getType().getDeclaredConstructor().newInstance();
            }
            int startRow = refCell.getRow();
            ParameterizedType type = (ParameterizedType) field.getGenericType();
            Class<?> paramCls = (Class<?>) type.getActualTypeArguments()[0];
            List<Field> rowCellRefFields = getRowCellRefFields(paramCls);
            boolean isLastRow = false;
            while (isLastRow == false) {
                row = sheet.getRow(startRow);
                if (row == null) {
                    break;
                }
                isLastRow = true;
                Object paramObj = paramCls.getDeclaredConstructor().newInstance();
                int startCol = refCell.getCol();
                for (Field f : rowCellRefFields) {
                    Cell cell = row.getCell(startCol);
                    if (cell == null) {
                        startCol++;
                        continue;
                    }
                    Object val = getCellValue(cell);
                    if (val != null) {
                        isLastRow = false;
                        if (nConverter.isNumber(val.getClass())) {
                            Object dVal = nConverter.Convert(val, f.getType());
                            PlainObjectOperator.runSetter(paramObj, field.getName(), dVal);
                        } else if (tConverter.isTime(val.getClass())) {
                            Object dt = tConverter.Convert(val, f.getType());
                            PlainObjectOperator.runSetter(paramObj, f.getName(), dt);
                        } else {
                            if (field.getType() == Boolean.class) {
                                PlainObjectOperator.runSetter(paramObj, f.getName(), Boolean.valueOf(val.toString()));
                            } else {
                                PlainObjectOperator.runSetter(paramObj, f.getName(), val.toString());
                            }
                        }
                    }
                    startCol++;
                }
                startRow++;
                if (rObj instanceof Collection) {
                    ((Collection) rObj).add(paramObj);
                }
            }
            PlainObjectOperator.runSetter(result, field.getName(), rObj);
        }
        return result;
    }
    
    private List<Field> getRowCellRefFields(Class<?> cls) {
        List<Field> ret = new ArrayList<Field>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                ret.add(f);
            }
        }
        return ret;
    }

    private Map<String, Field> getRowRefField(Class<?> cls) {
        Map<String, Field> ret = new HashMap<String, Field>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                String rowRef = info.rowRef();
                if (rowRef != null && !"".equals(rowRef)) {
                    ret.put(rowRef, f);
                }
            }
        }
        return ret;
    }

    private Map<String, Field> getCellRefField(Class<?> cls) {
        Map<String, Field> ret = new HashMap<String, Field>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                String cellRef = info.cellRef();
                if (cellRef != null && !"".equals(cellRef)) {
                    ret.put(cellRef, f);
                }
            }
        }
        return ret;
    }

    @Override
    public <T> List<T> parseList(Workbook wb, Class<T> cls) throws Exception {
        List<T> ret = new ArrayList<T>();
        int num = wb.getNumberOfSheets();
        for (int i = 0; i < num; i++) {
            Sheet sheet = wb.getSheetAt(i);
            T obj = parse(sheet, cls);
            ret.add(obj);
        }
        return ret;
    }
    
    
    public <T> Workbook export(T data, Class<T> cls) throws Exception {
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet();
        String sheetName = (String) PlainObjectOperator.runGetter(data, "sheetName");
        if (sheetName != null && !"".equals(sheetName.trim())) {
            wb.setSheetName(0, sheetName);
        }
        export(wb, sheet, data, cls);
        return wb;
    }
    
    public <T> Workbook exportList(List<T> list, Class<T> cls) throws Exception {
        Workbook wb = new XSSFWorkbook();
        int sheetIdx = 0;
        for (T data : list) {
            Sheet sheet = wb.createSheet();
            String sheetName = (String) PlainObjectOperator.runGetter(data, "sheetName");
            if (sheetName != null && !"".equals(sheetName.trim())) {
                wb.setSheetName(sheetIdx, sheetName);
            }
            export(wb, sheet, data, cls);
            sheetIdx++;
        }
        return wb;
    }
    
    public <T> Workbook export(File file, T data, Class<T> cls) throws Exception {
        Workbook wb = getWorkbook(file);
        String sheetName = (String) PlainObjectOperator.runGetter(data, "sheetName");
        if (sheetName != null && !"".equals(sheetName.trim())) {
            wb.setSheetName(0, sheetName);
        }
        export(wb, wb.getSheetAt(0), data, cls);
        return wb;
    }
    
    public <T> Workbook exportList(File file, List<T> list, Class<T> cls) throws Exception {
        Workbook wb = getWorkbook(file);
        if (list.size() > 1) {
            for (int i = 0; i < (list.size() - 1); i++) {
                wb.cloneSheet(0);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            String sheetName = (String) PlainObjectOperator.runGetter(list.get(i), "sheetName", String.class);
            if (sheetName != null && !"".equals(sheetName.trim())) {
                wb.setSheetName(i, sheetName);
            }
            export(wb, wb.getSheetAt(i), list.get(i), cls);
        }
        return wb;
    }
    
    public <T> void export(Workbook wb, Sheet sheet, T data, Class<T> cls) throws Exception {
        CellStyle dateStyle = getDateStyle(wb);
        Map<String, String> titleRefValueMap = getTitleRefValue(cls);
        for (Entry<String, String> entry : titleRefValueMap.entrySet()) {
            String title = entry.getKey();
            String ref = entry.getValue();
            CellReference refCell = new CellReference(ref);
            Row row = sheet.getRow(refCell.getRow());
            if (row == null) {
                row = sheet.createRow(refCell.getRow());
            }
            Cell cell = row.getCell(refCell.getCol());
            if (cell == null) {
                cell = row.createCell(refCell.getCol());
            }
            setCellValue(cell, title, dateStyle);
        }
        Map<String, Object> cellRefValueMap = getCellRefValue(data);
        for (Entry<String, Object> entry : cellRefValueMap.entrySet()) {
            String ref = entry.getKey();
            Object value = entry.getValue();
            CellReference refCell = new CellReference(ref);
            Row row = sheet.getRow(refCell.getRow());
            if (row == null) {
                row = sheet.createRow(refCell.getRow());
            }
            Cell cell = row.getCell(refCell.getCol());
            if (cell == null) {
                cell = row.createCell(refCell.getCol());
            }
            setCellValue(cell, value, dateStyle);
        }
        Map<String, String> rowTitleRefValueMap = getRowTitleRefValue(cls);
        for (Entry<String, String> entry : rowTitleRefValueMap.entrySet()) {
            String ref = entry.getKey();
            String title = entry.getValue();
            CellReference refCell = new CellReference(ref);
            Row row = sheet.getRow(refCell.getRow());
            if (row == null) {
                row = sheet.createRow(refCell.getRow());
            }
            Cell cell = row.getCell(refCell.getCol());
            if (cell == null) {
                cell = row.createCell(refCell.getCol());
            }
            setCellValue(cell, title, dateStyle);
        }
        Map<String, List<Object>> rowRefValueMap = getRowRefValue(data);
        for (Entry<String, List<Object>> entry : rowRefValueMap.entrySet()) {
            String ref = entry.getKey();
            List<Object> list = entry.getValue();
            CellReference refRow = new CellReference(ref);
            int startRow = refRow.getRow();
            int startCol = refRow.getCol();
            if (list != null && list.size() > 0) {
                int rowIdx = startRow;
                for (Object record : list) {
                    int colIdx = startCol;
                    List<Object> values = getListValues(record);
                    for (Object value : values) {
                        Row row = sheet.getRow(rowIdx);
                        if (row == null) {
                            row = sheet.createRow(rowIdx);
                        }
                        Cell cell = row.getCell(colIdx);
                        if (cell == null) {
                            cell = row.createCell(colIdx);
                        }
                        setCellValue(cell, value, dateStyle);
                        colIdx++;
                    }
                    rowIdx++;
                }
            }
        }
    }
    
    private Map<String, String> getRowTitleRefValue(Class<?> cls) {
        Map<String, String> ret = new HashMap<String, String>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                String rowTitleRef = info.rowTitleRef();
                if (!"".equals(rowTitleRef)) {
                    CellReference ref = new CellReference(rowTitleRef);
                    int rowIdx = ref.getRow();
                    int colIdx = ref.getCol();
                    ParameterizedType type = (ParameterizedType) f.getGenericType();
                    Class<?> paramCls = (Class<?>) type.getActualTypeArguments()[0];
                    List<String> titles = getTitleList(paramCls);
                    for (String title : titles) {
                        CellReference paramTitleRef = new CellReference(rowIdx, colIdx);
                        ret.put(paramTitleRef.formatAsString(), title);
                        colIdx++;
                    }
                }
            }
        }
        return ret;
    }

    private List<String> getTitleList(Class<?> cls) {
        List<String> ret = new ArrayList<String>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                String title = info.title();
                if (!"".equals(title)) {
                    ret.add(title);
                }
            }
        }
        return ret;
    } 
    
    private Map<String, String> getTitleRefValue(Class<?> cls) {
        Map<String, String> ret = new HashMap<String, String>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                String ref = info.cellTitleRef();
                if (!"".equals(ref)) {
                    ret.put(f.getName(), ref);
                }
            }
        }
        return ret;
    }

    private List<Object> getListValues(Object record) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        List<Object> list = new ArrayList<Object>();
        Field[] fields = record.getClass().getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                Object val = PlainObjectOperator.runGetter(record, f.getName());
                list.add(val);
            }
        }
        return list;
    }

    private Map<String, Object> getCellRefValue(Object data) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Map<String, Object> ret = new HashMap<String, Object>();
        if (data == null) {
            return ret;
        }
        Map<String, String> fieldCellRefMap = getFieldCellRefMap(data.getClass());
        for (Entry<String, String> entry : fieldCellRefMap.entrySet()) {
            String field = entry.getKey();
            String ref = entry.getValue();
            Object val = PlainObjectOperator.runGetter(data, field);
            ret.put(ref, val);
        }
        return ret;
    }
    
    @SuppressWarnings("unchecked")
    private Map<String, List<Object>> getRowRefValue(Object data) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        Map<String, List<Object>> ret = new HashMap<String, List<Object>>();
        if (data == null) {
            return ret;
        }
        Map<String, String> fieldRowRefMap = getFieldRowRefMap(data.getClass());
        for (Entry<String, String> entry : fieldRowRefMap.entrySet()) {
            String field = entry.getKey();
            String ref = entry.getValue();
            Object val = PlainObjectOperator.runGetter(data, field);
            if (val != null) {
                if (val instanceof List) {
                    ret.put(ref, (List<Object>) val);
                } else {
                    throw new RuntimeException(val.getClass().getName() + " is not List!");
                }
            }
        }
        return ret;
    }

    private Map<String, String> getFieldRefMap(Class<?> cls, String refName) {
        Map<String, String> ret = new HashMap<String, String>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            PoiInfo info = f.getAnnotation(PoiInfo.class);
            if (info != null) {
                String ref = null;
                if ("cellRef".equalsIgnoreCase(refName)) {
                    ref = info.cellRef();
                } else if ("rowRef".equalsIgnoreCase(refName)) {
                    ref = info.rowRef();
                } else {
                    throw new RuntimeException("unknown refName: " + refName + "!");
                }
                if (!"".equals(ref)) {
                    ret.put(f.getName(), ref);
                }
            }
        }
        return ret;
    }
    
    private Map<String, String> getFieldRowRefMap(Class<?> cls) {
        return getFieldRefMap(cls, "rowRef");
    }

    private Map<String, String> getFieldCellRefMap(Class<?> cls) {
        return getFieldRefMap(cls, "cellRef");
    }

}
