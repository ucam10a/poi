package poi;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ObjectConstructor {

    /**
     * This method will create a new target object then copy all field from
     * source to target if they are the same Note: object hash code changes, so
     * it is a new object
     * 
     * @param source
     *            source object
     * @param target
     *            target class
     * @return new target object
     */
    public static <T> T construct(Object source, Class<T> target) {

        T result = null;
        if (source == null) return null;
        try {
            result = target.newInstance();
            List<String> clsTFieldNames = new ArrayList<String>();
            Class<?> cls = target;
            // print field
            while (cls != null) {
                Field[] fields = cls.getDeclaredFields();
                for (Field f : fields) {
                    clsTFieldNames.add(f.getName());
                }
                cls = cls.getSuperclass();
            }
            for (String name : clsTFieldNames) {
                Object value = PlainObjectOperator.runGetter(source, name);
                if (value != null) {
                    PlainObjectOperator.runSetter(result, name, value);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;

    }
    
}
