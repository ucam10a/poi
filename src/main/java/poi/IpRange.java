package poi;

import java.util.List;

@Process(methods={"ipToCidr"})
public class IpRange {
    
    @PoiInfo(title="IP Range")
    private String range;
    
    @PoiInfo(title="Env")
    private String env;
    
    @PoiInfo(title="CIDR")
    private String cidr;
    
    @PoiInfo(title="Remark")
    private String remark;

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getCidr() {
        return cidr;
    }

    public void setCidr(String cidr) {
        this.cidr = cidr;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    public void ipToCidr() {
        if (range == null) {
            return;
        }
        String[] tokens = range.split("-");
        if (tokens.length > 2) {
            throw new RuntimeException("ip range should not over two, start and end");
        }
        String startIp = tokens[0].trim();
        String endIp = tokens[1].trim();
        List<String> cidrs = CIDRTool.range2cidrlist(startIp, endIp);
        String ret = cidrs.toString();
        this.cidr = ret.substring(1, ret.length() - 1);
    }
       
}
