package poi;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;

/**
 * Data Record Processor and Converter
 * 
 * @author Yung-Long Li
 *
 */
public class DataProcessor {

    /**
     * Process data collections
     * 
     * @param collections data collections
     * @param args arguments
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public static void processCollection(Collection<?> collections, Object... args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        for (Object obj : collections) {
            process(obj, args);
        }
    }
    
    /**
     * Process data
     * 
     * @param obj data object
     * @param args arguments
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public static void process(Object obj, Object... args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (obj == null) {
            return;
        }
        Process p = obj.getClass().getAnnotation(Process.class);
        if (p != null) {
            for (String method : p.methods()) {
                if (method != null && !"".equals(method)) {
                    int argNum = 0;
                    if (args != null) {
                        argNum = args.length;
                    }
                    Method m = findMethod(method, obj, argNum);
                    if (m.getGenericParameterTypes().length > 0 && args.length == 0) {
                        Object[] newArgs = new Object[m.getGenericParameterTypes().length];
                        m.invoke(obj, newArgs);
                    } else {
                        m.invoke(obj, args);
                    }
                }
            }
        }
    }
    
    /**
     * Data field converter
     * 
     * @param obj data object
     * @param args arguments
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     */
    public static void convert(Object obj, Object... args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (obj == null) {
            return;
        }
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field f : fields) {
            Converter c = f.getAnnotation(Converter.class);
            if (c != null) {
                String method = c.method();
                if (method != null && !"".equals(method)) {
                    int argNum = 0;
                    if (args != null) {
                        argNum = args.length;
                    }
                    Method m = findMethod(method, obj, argNum + 2);
                    m.invoke(obj, f.getName(), f.getType(), args);
                }
            }
        }
    }
    
    private static Method findMethod(String methodName, Object obj, int argNum) {
        for (Method method : obj.getClass().getMethods()) {
            if (method.getName().equalsIgnoreCase(methodName)) {
                Argument argu = method.getAnnotation(Argument.class);
                if (argu != null && argu.allowNoArg() == true) {
                    return method;
                }
                if (argNum == method.getParameters().length) {
                    return method;
                } else {
                    throw new RuntimeException("Method: " + methodName + " parameter size not match! want " + argNum + ", but method parameter size is " + method.getParameters().length);
                }
            }
        }
        return null;
    }
    
}
