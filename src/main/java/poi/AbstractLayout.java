package poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DateUtil;

public abstract class AbstractLayout {

    public final static NumberConverter nConverter = new NumberConverter();
    public final static TimeConverter tConverter = new TimeConverter();
    
    public static void setCellValue(Cell cell, String val) {
        setCellValue(cell, val, null);
    }
    
    public static void setCellValue(Cell cell, Object val, CellStyle dateStyle) {
        if (val == null) {
            return;
        }
        if (nConverter.isNumber(val.getClass())) {
            Double dVal = (Double) nConverter.Convert(val, Double.class);
            cell.setCellValue(dVal);
        } else if (tConverter.isTime(val.getClass())) {
            Date dt = (Date) tConverter.Convert(val, Date.class);
            cell.setCellValue(dt);
            if (dateStyle != null) {
                cell.setCellStyle(dateStyle);
            }
        } else {
            cell.setCellValue(val.toString());
        }
    }
    
    public static CellStyle getDateStyle(Workbook wb) {
        CellStyle cellStyle = wb.createCellStyle();
        CreationHelper createHelper = wb.getCreationHelper();
        cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("yyyy/m/d h:mm:ss"));
        return cellStyle;
    }
    
    public static Object getCellValue(Cell cell) {
        CellType type = cell.getCellType();
        if (type == CellType.BLANK) {
            return null;
        } else if (type == CellType.BOOLEAN) {
            return cell.getBooleanCellValue();
        } else if (type == CellType.NUMERIC) {
            if (DateUtil.isCellDateFormatted(cell)) {
                return cell.getDateCellValue();
            }
            return cell.getNumericCellValue();
        } else if (type == CellType.STRING) {
            return cell.getStringCellValue();
        }
        return null;
    }
    
    public static void setBeanValue(Object bean, String fieldName, Object val) throws Exception {
        if (val == null) {
            // ignore
        } else {
            Class<?> type = null;
            try {
                type = bean.getClass().getDeclaredField(fieldName).getType();
                Object value = val;
                if (nConverter.isNumber(type)) {
                    value = nConverter.Convert(val, type);
                } else if (tConverter.isTime(type)) {
                    value = tConverter.Convert(val, type);
                } else if (type == String.class) {
                    value = val.toString();
                }
                PlainObjectOperator.runSetter(bean, fieldName, value);
            } catch (Exception e) {
                throw new Exception("field:" + fieldName + ", type:" + type + ", " + e.toString());
            }
        }
    }
    
    public static int getLastRow(Sheet sheet, int colMax) {
        int lastRow = sheet.getLastRowNum();
        for (int i = 0; i < lastRow; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                return i;
            }
            boolean isEmptyRow = true;
            for (int j = 0; j < colMax; j++) {
                Cell cell = row.getCell(j);
                if (cell == null) {
                    continue;
                }
                if (cell.getCellType() != CellType.BLANK) {
                    if (cell.getCellType() == CellType.STRING) {
                        String val = cell.getStringCellValue();
                        if (!"".equals(val)) {
                            isEmptyRow = false;
                            break;
                        }
                    } else {
                        isEmptyRow = false;
                        break;
                    }
                }
            }
            if (isEmptyRow == false) {
                continue;
            } else {
                return i;
            }
        }
        return lastRow;
    }
    
    public static Sheet cloneSheet(Workbook wb, Sheet source, String newSheetName) {
        int idx = wb.getSheetIndex(source);
        Sheet sheet = wb.cloneSheet(idx);
        int newIdx = wb.getSheetIndex(sheet);
        wb.setSheetName(newIdx, newSheetName);
        return sheet;
    }
    
    public static void hideRow(Row row) {
        row.setZeroHeight(true);  
    }
    
    public static void hideColumn(Sheet sheet, int colIdx) {
        sheet.setColumnHidden(colIdx, true);
    }
    
    public static void deleteSheet(Workbook wb, Sheet deleteSheet) {
        int idx = wb.getSheetIndex(deleteSheet);
        wb.removeSheetAt(idx);
    }
    
    public static void saveFile(Workbook wb, File file) throws IOException {
        
        FileOutputStream outputStream = new FileOutputStream(file);
        try {
            wb.write(outputStream);
        } finally {
            outputStream.close();
        }
        
    }
    
    public static Workbook getWorkbook(File file) throws InvalidFormatException, FileNotFoundException, IOException {
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            return WorkbookFactory.create(in);
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }
    
    public static Workbook getWorkbook(InputStream in) throws InvalidFormatException, IOException {
        try {
            return WorkbookFactory.create(in);
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }
    
}

