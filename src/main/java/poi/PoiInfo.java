package poi;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// on field level
public @interface PoiInfo {

    String title() default "";
    
    String cellTitleRef() default "";
    
    String cellRef() default "";
    
    String rowTitleRef() default "";

    String rowRef() default "";
    
    short width() default -1;
    
    short height() default -1;
}
