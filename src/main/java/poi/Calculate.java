package poi;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellReference;

/**
 * Define first sheet as variable sheet and second sheet A1 value as result
 * 
 * @author Yung-Long Li
 *
 */
public class Calculate {

    private static NumberConverter nConverter = new NumberConverter();
    private static TimeConverter tConverter = new TimeConverter();
    
    public static void setCellValue(Cell cell, Object value) {
        if (value != null) {
            if (nConverter.isNumber(value.getClass())) {
                Double val = (Double) nConverter.Convert(value, Double.class);
                cell.setCellValue(val.doubleValue());
            } else if (tConverter.isTime(value.getClass())) {
                Date val = (Date) tConverter.Convert(value, Date.class);
                cell.setCellValue(val);
            } else if (value.getClass() == String.class) {
                String val = (String) value;
                cell.setCellValue(val);
            }
        }
    }
    
    public static Date getCellDateValue(Cell cell) {
        if (DateUtil.isCellDateFormatted(cell)) {
            return cell.getDateCellValue();
        } else {
            throw new RuntimeException("cell value is not date!");
        }
    }
    
    public static void print(CellValue cellValue) {
        switch (cellValue.getCellType()) {
        case BOOLEAN:
            System.out.println(cellValue.getBooleanValue());
            break;
        case NUMERIC:
            System.out.println(cellValue.getNumberValue());
            break;
        case STRING:
            System.out.println(cellValue.getStringValue());
            break;
        case BLANK:
            break;
        case ERROR:
            break;

        // CELL_TYPE_FORMULA will never happen
        case FORMULA:
            break;
        default:
            break;
        }
    }
    
    private static void clearSheet(Sheet sheet, int startRow) {
        int rowNum = sheet.getLastRowNum();
        for (int rowIdx = startRow; rowIdx < rowNum; rowIdx++) {
            Row row = sheet.getRow(rowIdx);
            if (row == null) {
                continue;
            }
            int colNum = row.getLastCellNum();
            for (int colIdx = 0; colIdx < colNum; colIdx++) {
                Cell cell = row.getCell(colIdx);
                if (cell == null) {
                    continue;
                }
                cell.setBlank();
            }
        }
    }
    
    public static CellValue calculateSingleRow(Workbook wb, Map<String, Object> dataSource) throws Exception {
        
        Sheet variable = wb.getSheetAt(0);
        clearSheet(variable, 1);
        int col = 0;
        Row varRow = variable.getRow(0);
        Row valRow = variable.getRow(1);
        if (valRow == null) {
            valRow = variable.createRow(1);
        }
        Cell varCell = varRow.getCell(col);
        String varName = varCell.getStringCellValue();
        while (varName != null && !"".equals(varName.trim())) {
            Cell valCell = valRow.getCell(col);
            if (valCell == null) {
                valCell = valRow.createCell(col);
            }
            setCellValue(valCell, dataSource.get(varName));
            col = col + 1;
            varCell = varRow.getCell(col);
            if (varCell == null) {
                break;
            }
            varName = varCell.getStringCellValue();
        }
        
        Sheet result = wb.getSheetAt(1);
        clearSheet(result, 0);
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

        // suppose your formula is in B3
        CellReference resultCellRef = new CellReference("A1");
        Row row = result.getRow(resultCellRef.getRow());
        Cell resultCell = row.getCell(resultCellRef.getCol()); 

        CellValue cellValue = evaluator.evaluate(resultCell);
        return cellValue;
        
    }
    
    public static CellValue calculateMultiRows(Workbook wb, Map<String, List<Object>> dataSource) throws Exception {
        
        Sheet variable = wb.getSheetAt(0);
        clearSheet(variable, 1);
        int colIdx = 0;
        Row varRow = variable.getRow(0);
        Cell varCell = varRow.getCell(colIdx);
        String varName = varCell.getStringCellValue();
        while (varName != null && !"".equals(varName.trim())) {
            List<Object> valList = dataSource.get(varName);
            int rowIdx = 1;
            if (valList != null) {
                for (Object val : valList) {
                    Row valRow = variable.getRow(rowIdx);
                    if (valRow == null) {
                        valRow = variable.createRow(rowIdx);
                    }
                    Cell valCell = valRow.getCell(colIdx);
                    if (valCell == null) {
                        valCell = valRow.createCell(colIdx);
                    }
                    setCellValue(valCell, val);
                    rowIdx = rowIdx + 1;
                }
            }
            colIdx = colIdx + 1;
            varCell = varRow.getCell(colIdx);
            if (varCell == null) {
                break;
            }
            varName = varCell.getStringCellValue();
        }
        
        Sheet result = wb.getSheetAt(1);
        clearSheet(result, 0);
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

        CellReference resultCellRef = new CellReference("A1");
        Row row = result.getRow(resultCellRef.getRow());
        Cell resultCell = row.getCell(resultCellRef.getCol()); 

        CellValue cellValue = evaluator.evaluate(resultCell);
        return cellValue;
        
    }
    
}