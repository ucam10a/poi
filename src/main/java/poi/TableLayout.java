package poi;

import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public interface TableLayout {

    public abstract <T> List<T> parseList(Sheet sheet, Class<T> cls) throws Exception;
    
    public abstract <T> Workbook exportList(List<T> list, Class<T> cls) throws Exception;
    
}
