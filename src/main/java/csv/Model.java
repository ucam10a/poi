package csv;

import java.math.BigDecimal;

public class Model {

    @CsvInfo(title="Make")
    private String make;
    
    @CsvInfo(title="Model")
    private String model;
    
    @CsvInfo(title="Description")
    private String description;
    
    @CsvInfo(title="Price", scale=2)
    private BigDecimal price;
    
    @CsvInfo(title="Remark")
    private String remark;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    
}
