package csv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import poi.AppLogger;
import poi.NumberConverter;
import poi.ObjectConverter;
import poi.PlainObjectOperator;
import poi.TimeConverter;

/**
 * Simple csv paser
 * 
 * @author Yung-Long Li
 *
 */
public class EasyCsvParser {
    
    private static final AppLogger logger = AppLogger.getLogger(EasyCsvParser.class);

    private char delimeter = ',';
    private static final char doubleQuote = '"';
    private static final char lf = (char) 10;
    
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");
    
    private String charset = "UTF-8";
    private boolean withHeader = false;
    private boolean skipError = false;
    private List<String> headers = new ArrayList<String>();
    
    private static NumberConverter nConverter = new NumberConverter();
    private static TimeConverter tConverter = new TimeConverter();
    
    public void setWithHeader(boolean withHeader) {
        this.withHeader = withHeader;
    }
    
    private List<String> lines = new ArrayList<String>();
    
    public EasyCsvParser(File file) {
        try {
            read(file, charset);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public EasyCsvParser(List<String> lines) {
        read(lines);
    }
    
    private void read(File file, String charset) throws IOException {
        this.lines.clear();
        headers.clear();
        InputStreamReader isr = new InputStreamReader(new FileInputStream(file), charset);
        BufferedReader br = new BufferedReader(isr);
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) this.lines.add(line);
                line = br.readLine();
            }
        } finally {
            br.close();
        }
    }
    
    private void read(List<String> lines) {
        this.lines.clear();
        this.headers.clear();
        this.lines.addAll(lines);
    }
    
    private static boolean isNumeric(String str) {
        boolean ret = str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
        if (ret == true) {
            if (str.startsWith("0") && str.indexOf(".") < 0) {
                return false;
            }
        }
        return ret;
    }
    
    public void setDelimeter(char delimeter) {
        this.delimeter = delimeter;
    }
    
    public void setSkipError(boolean skipError) {
        this.skipError = skipError;
    }
    
    public List<List<Object>> toList() {
    	  headers.clear();
        int columnSize = -1;
        List<List<Object>> result = new ArrayList<List<Object>>();
        int lineIdx = 0;
        boolean firstElement = true;
        while (lineIdx < lines.size()) {
            String line = lines.get(lineIdx);
            List<Object> list = new ArrayList<Object>();
            boolean escape = false;
            boolean end = false;
            boolean appendEmpty = false;
            StringBuilder sb = new StringBuilder();
            int idx = 0;
            while(idx < line.length()) {
                char c = line.charAt(idx);
                char nextC = lf;
                if (idx + 1 < line.length()) {
                    nextC = line.charAt(idx + 1);
                }
                if (escape == false && c == doubleQuote) {
                    escape = true;
                } else if (escape == true && c != doubleQuote && idx + 1 == line.length()) {
                    lineIdx++;
                    if (lineIdx < lines.size()) {
                        line = line + lines.get(lineIdx);
                    }
                    sb.append(c);
                    sb.append(LINE_SEPARATOR);
                } else if (escape == true && c == doubleQuote && nextC != doubleQuote) {
                    escape = false;
                    if (idx + 1 == line.length()) {
                        end = true;
                    }
                } else if (escape == true && c == doubleQuote && nextC == doubleQuote) {
                    idx++;
                    sb.append("\"");
                } else if (escape == false && idx + 1 == line.length()) {
                    if (c != delimeter) {
                        sb.append(c);
                    } else {
                        appendEmpty = true;
                    }
                    end = true;
                } else if (escape == false && c == delimeter) {
                    end = true;
                } else {
                    sb.append(c);
                }
                if (end == true) {
                    String component = sb.toString();
                    boolean isNum = isNumeric(component);
                    if (isNum) {
                        BigDecimal num = new BigDecimal(component);
                        list.add(num);
                    } else {
                        list.add(component);
                    }
                    sb = new StringBuilder();
                    end = false;
                    escape = false;
                }
                idx++;
            }
            if (appendEmpty == true) {
                list.add("");
            }
            if (columnSize == -1) {
                columnSize = list.size();
            } else {
                if (columnSize != list.size()) {
                    if (skipError) {
                        logger.warn("Csv Column size[" + list.size() + "] not match " + columnSize + ", list: " + list);
                    } else {
                        throw new RuntimeException("Csv Column size[" + list.size() + "] not match " + columnSize + ", list: " + list);
                    }
                }
            }
            if (withHeader != true || firstElement == false) {
                result.add(list);
            } else {
                for (Object header : list) {
                    headers.add(header.toString());
                }
            }
            lineIdx++;
            firstElement = false;
        }
        return result;
    }
    
    private List<Map<String, Object>> parseMap() {
    	List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<List<Object>> lists = toList();
        for (List<Object> list : lists) {
        	Map<String, Object> map = new HashMap<String, Object>();
            for (int i = 0; i < headers.size(); i++) {
                map.put(headers.get(i), list.get(i));
            }
            result.add(map);
        }
        return result;
    }
    
    private <T> T parse(Class<T> clsV, Map<String, Object> map) throws Exception {
        T obj = clsV.getDeclaredConstructor().newInstance();
        List<Field> fields = new ArrayList<Field>();
        List<String> columnNames = new ArrayList<String>();
        Class<?> cls = clsV;
        while (cls != Object.class) {
            for (Field f : cls.getDeclaredFields()) {
                CsvInfo c = f.getAnnotation(CsvInfo.class);
                if (c != null) {
                    fields.add(f);
                    columnNames.add(c.title());
                }
            }
            cls = cls.getSuperclass();
        }
        Set<String> columnLabels = map.keySet();
        for (int i = 0; i < fields.size(); i++) {
            String column = columnNames.get(i);
            Field f = fields.get(i);
            Type fType = f.getType();
            Class<?> clsF = ObjectConverter.getClass(fType);
            if (columnLabels.contains(column)) {
                Object value = map.get(column);
                if (value != null) {
                    if (clsF == String.class) {
                        value = value.toString();
                    } else if (nConverter.isNumber(fType)) {
                        value = nConverter.Convert(value, clsF);
                    } else if (tConverter.isTime(fType)) {
                    	if (value.getClass() == String.class) {
                    		CsvInfo c = f.getAnnotation(CsvInfo.class);
                    		SimpleDateFormat sdf = new SimpleDateFormat(c.format());
                    		value = sdf.parse(value.toString());
                    	}
                    	value = tConverter.Convert(value, clsF);
                    }
                    PlainObjectOperator.runSetter(obj, f.getName(), value);
                }    
            }
        }
        return obj;
    }
    
    public <T> List<T> parse(Class<T> clsV) throws Exception {
        if (this.headers.size() == 0) {
            this.headers.addAll(getCsvInfoHeaders(clsV));
        }
        List<Map<String, Object>> listMap = parseMap();
        List<T> ret = new ArrayList<T>();
        if (listMap != null && listMap.size() > 0) {
            for (Map<String, Object> map : listMap) {
                T obj = parse(clsV, map);
                ret.add(obj);
            }
        }
        return ret;
    }
    
    public static <T> void exportCsv(String outFilePath, List<T> list, Class<T> cls) throws Exception {
        exportCsv(outFilePath, list, cls, false);
    }
    
    private List<String> getCsvInfoHeaders(Class<?> cls) {
        List<String> ret = new ArrayList<String>();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            CsvInfo info = f.getAnnotation(CsvInfo.class);
            if (info != null) {
                if (!"".equals(info.title())) {
                    ret.add(info.title());
                } else {
                    ret.add(f.getName());
                }
            }
        }
        return ret;
    }
    
    private static String getHeader(Class<?> cls) {
        StringBuilder sb = new StringBuilder();
        Field[] fields = cls.getDeclaredFields();
        for (Field f : fields) {
            CsvInfo info = f.getAnnotation(CsvInfo.class);
            if (info != null) {
                if (!"".equals(info.title())) {
                    String title = escapeDoubleQuote(info.title());
                    sb.append("\"").append(title).append("\"").append(",");
                } else {
                    sb.append("\"").append(f.getName()).append("\"").append(",");
                }
            }
        }
        String ret = sb.toString();
        if (ret.length() > 0) {
            ret = ret.substring(0, ret.length() - 1);
        }
        return ret;
    }
    
    private static String escapeDoubleQuote(String input) {
        if (input == null) return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == doubleQuote) {
                sb.append(c).append(c);
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static <T> void exportCsv(String outFilePath, List<T> list, Class<T> cls, boolean withHeader) throws Exception {
        StringBuilder sb = new StringBuilder();
        if (withHeader) {
            String header = getHeader(cls) + LINE_SEPARATOR;
            sb.append(header);
        }
        for (Object obj : list) {
            StringBuilder line = new StringBuilder();
            Field[] fields = cls.getDeclaredFields();
            for (Field f : fields) {
                CsvInfo info = f.getAnnotation(CsvInfo.class);
                if (info != null) {
                    Object val = PlainObjectOperator.runGetter(f, obj);
                    if (val == null) {
                        line.append("\"\"").append(",");
                    } else {
                        if (val instanceof String) {
                            line.append("\"" + escapeDoubleQuote(val.toString()) + "\"").append(",");
                        } else if (nConverter.isNumber(f.getType())) {
                            BigDecimal decimal = (BigDecimal) nConverter.Convert(val, BigDecimal.class);
                            decimal = decimal.setScale(info.scale(), info.round());
                            line.append(decimal.toPlainString()).append(",");
                        } else if (tConverter.isTime(f.getType())) {
                            SimpleDateFormat sdf = new SimpleDateFormat(info.format());
                            java.util.Date date = (Date) tConverter.Convert(val, java.util.Date.class);
                            line.append("\"").append(sdf.format(date)).append("\"").append(",");
                        } else {
                            throw new RuntimeException("Not support type: " + f.getType() + ", only String, number and time!");
                        }
                    }
                }
            }
            String lineString = line.toString();
            if (lineString.length() > 0) {
                lineString = lineString.substring(0, lineString.length() - 1);
            }
            sb.append(lineString + LINE_SEPARATOR);
        }
        String ret = sb.toString();
        writeTextToFile(outFilePath, ret);
    }
    
    private static File writeTextToFile(String file, String inputText) {
        BufferedWriter writer = null;
        File logFile = null;
        try {
            logFile = new File(file);
            writer = new BufferedWriter(new FileWriter(logFile));
            writer.write(inputText);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
        return logFile;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }
    
}
