package csv;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
// on field level
public @interface CsvInfo {

    String title() default "";
    
    int scale() default 4;
    
    /**
     * ROUND_UP          = 0<br>
     * ROUND_DOWN        = 1<br>
     * ROUND_CEILING     = 2<br>
     * ROUND_FLOOR       = 3<br>
     * ROUND_HALF_UP     = 4<br>
     * ROUND_HALF_DOWN   = 5<br>
     * ROUND_HALF_EVEN   = 6<br>
     * ROUND_UNNECESSARY = 7
     */
    int round() default 4;
    
    String format() default "yyyyMMdd HH:mm:ss.SSS";

}
