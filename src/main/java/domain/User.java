package domain;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import poi.Argument;
import poi.PoiInfo;
import poi.Process;

@Process(methods= {"checkAccountValid"})
public class User {

    @PoiInfo(title="username")
    private String username;
    
    @PoiInfo(title="password")
    private String password;

    private boolean allowLogin = true;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @Argument(allowNoArg=true)
    public void checkAccountValid(List<String> passwordHistrory) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        if (passwordHistrory != null && passwordHistrory.contains(password)) {
            this.allowLogin = false;
        }
        if (password == null || password.length() < 6) {
            this.allowLogin = false;
        }
    }

    public boolean isAllowLogin() {
        return allowLogin;
    }

}
