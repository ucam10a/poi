package domain;

import poi.PoiInfo;

public class Student {

    @PoiInfo(width=50)
    private String id;
    
    @PoiInfo(width=80)
    private String name;
    
    @PoiInfo()
    private String colledge;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColledge() {
        return colledge;
    }

    public void setColledge(String colledge) {
        this.colledge = colledge;
    }
    
}
