package domain;

import java.util.ArrayList;
import java.util.List;

import poi.PoiInfo;

public class ClassData {
    
    private String sheetName;
    
    @PoiInfo(title="Teacher", cellTitleRef="A1", cellRef="B1")
    private String teacherName;
    
    @PoiInfo(title="Office No.", cellTitleRef="A2", cellRef="B2")
    private String office;
    
    @PoiInfo(title="Tel No.", cellTitleRef="A3", cellRef="B3")
    private String tel;
    
    @PoiInfo(rowRef="A6")
    private List<Student> students = new ArrayList<Student>();

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }
    
}
